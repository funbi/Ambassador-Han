
##  Han

## Description
A helper services that send network requests on behalf of a legacy or heritage consumer service or application. This service can be thought of as an out-of-process proxy that is co-located with the client or as a standalone microservice.

This service can be used for offloading common client task such as monitoring, logging, routing, security and resilence patterns. It should be often used with legacy application that are difficult to modify or extend their networking capabilities.

## Badges
Add badges here

## Installation

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.


# Routes
| Name         | Endpoint    | Verb       | Description                                                      |
| -----------  | ----------- | ---------  |------------                                                      |
| Health Check | /health     |  Get       |  Gives information about the health of the application.          |
| Send Request | /request    |            |                                                                  | 

## Feature
- [ ] System Design
- [ ] Implement API key
- [ ] Configure HTTPS
- [x] Add health check
- [ ] Measure request latency
- [x] Dockerize App
- [ ] Write Unit Test and Generate Code Coverage
- [ ] Implement Pipeline with Automation Script(gitlab)
- [ ] Setup AWS Infrastructure 
- [ ] Retry with SQS
- [ ] Circuit Breaking
- [ ] Monitoring and Logging
- [ ] Metrics
- [ ] Security


## Project status
Ongoing


# Project References
- [ ] [Ambassador](https://dkbalachandar.wordpress.com/2021/05/10/cloud-design-pattern-ambassador-pattern/)
- [ ] [Ambassador](https://learn.microsoft.com/en-us/azure/architecture/patterns/ambassador)

## Pipelines todo

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
