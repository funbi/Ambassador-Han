FROM mcr.microsoft.com/dotnet/aspnet:7.0 as base
WORKDIR /app
# container port
# WHERE 80 IS HTTP AND 443 IS HTTPS
EXPOSE 80 443
ENV ASPNETCORE_ENVIRONMENT=Development
ENV ASPNETCORE_URLS=http://+:80


FROM mcr.microsoft.com/dotnet/sdk:7.0 AS publish
WORKDIR /src
COPY . ./
RUN dotnet restore
 # publish to the publish directory in the image --> src/publish
RUN dotnet publish -c Release -o publish


FROM base as final
WORKDIR /app
COPY --from=publish /src/publish .
ENTRYPOINT ["dotnet", "AmbassadorHan.dll"] 